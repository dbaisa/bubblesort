import React from 'react';
import Bubble from './bubble';
import BubblesCompare from './bubbles-compare';

class Bubbles extends React.Component {

    constructor(props){
        super(props);
        
        this.state = {
            bubbles:[],
            index:0
        };
    }

    createRandomArray = (num) =>{
        let tempArr = [];
        for(let i =0 ;i< num; i++){
            let random = Math.floor(Math.random() * num + 1);
            if (tempArr.indexOf(random) >= 0){
                while (tempArr.indexOf(random) >= 0) {
                    random = Math.floor(Math.random() * num + 1);
                  }
            }
            tempArr.push(random);
        }
        return tempArr;

    }

    printBubbles = (bubbles) => {
        return bubbles.map((bubble,index) => {
            let id = "bubble" + index
            return <Bubble key={bubble} number={bubble} index={id} />;
        })
    }


    sortBubbles = () => {
            let tempArr = [...this.state.bubbles]; 
            let length = tempArr.length;
            
            for(let i = this.state.index; i < length - 1; i++){
                    if(BubblesCompare(tempArr[i],tempArr[i+1]) === true){
                       
                            let swapedBubble = tempArr[i+1];
                            tempArr[i+1] = tempArr[i];
                            tempArr[i] = swapedBubble;
                            requestAnimationFrame(() => {
                                
                                this.swapBubbles(this.state.index);
                                setTimeout(function(){
                                    this.setState({bubbles: tempArr})
                                }.bind(this),1000);
                            });

                            return i;
                            
                    }
                }
                        
            return 0;
    }

    chekForSorted = (index = 0) => {
        
        let tempArr = [...this.state.bubbles];
                    
        let length = tempArr.length;
         
        for(let i = index; i < length - 1; i++){
            if(BubblesCompare(tempArr[i],tempArr[i+1]) === true){
               
                    let swapedBubble = tempArr[i+1];
                    tempArr[i+1] = tempArr[i];
                    tempArr[i] = swapedBubble;
                    return false;                
            }

        }
        console.log(tempArr);
        console.log("sorted!!!!");
        return true;
    }
    
    runAnimation = () =>{

        const interval = setInterval(function(){
                
                if(this.chekForSorted()){
                    clearInterval(interval);
                    return;
                }else{
                    console.log(this.state.bubbles);
                    console.log(this.state.index);
                    let index = this.sortBubbles();
                    this.setState({index});
                }      
                
            }.bind(this),2000)
        
            
    }    
    

    componentDidMount(){
        this.setState({bubbles: this.createRandomArray(10)},this.runAnimation());        
    }
    swapBubbles = (i = null) =>{
        console.log("iiiii:" + i);
        if(i === null){return};
        
        let bubble1 =  document.querySelectorAll(`#bubble${i}`)[0];
        console.log(bubble1);
    
        let bubble1Transform =  window
                                    .getComputedStyle(bubble1)
                                    .getPropertyValue('transform') !== "none" ?  window
                                    .getComputedStyle(bubble1)
                                    .getPropertyValue('transform').match(/(-?[0-9\.]+)/g)[4] : 0;
        
        let bubble2 = document.querySelectorAll(`#bubble${i + 1}`)[0];
        
        let bubble2Transform =  window
                                    .getComputedStyle(bubble2)
                                    .getPropertyValue('transform') !== "none" ?  window
                                    .getComputedStyle(bubble2)
                                    .getPropertyValue('transform').match(/(-?[0-9\.]+)/g)[4] : 0;
        
        let transformBubble1X = parseInt(bubble1Transform) + window.outerWidth > 600 ?  85 : 40 ;
        let transformBubble2X = parseInt(bubble2Transform) + window.outerWidth > 600 ?  -85 : -40 ;
            
         bubble1.style.transform = `translate(${transformBubble1X}px)`;
         bubble2.style.transform = `translate(${transformBubble2X}px)`; 

         console.log("swaped");
  
    }

    

    render(){
        let items = this.printBubbles(this.state.bubbles);
        return (
            <div className="bubble-sort">
                {items}
            </div>
        )
    }
    

}

export default Bubbles;