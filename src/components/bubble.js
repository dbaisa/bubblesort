import React from 'react';

class Bubble extends React.Component{

    render(){
        return(
            <div className="bubble" key={this.props.index} id={this.props.index}>
                {this.props.number}
            </div>
        )
    }
}

export default Bubble;