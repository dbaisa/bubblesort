import React from 'react';
import './App.css';
import Bubbles from './components/bubbles'

function App() {
  return (
    <div className="App">
      <Bubbles />
    </div>
  );
}

export default App;
